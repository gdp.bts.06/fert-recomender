import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from './pages/Home.vue';
import Page1 from './pages/Page1.vue';
import Page2 from './pages/Page2.vue';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    linkExactActiveClass: 'active',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/page1',
            name: 'page1',
            component: Page1
        },
        {
            path: '/page2',
            name: 'page2',
            component: Page2
        },
    ]
});

export default router;