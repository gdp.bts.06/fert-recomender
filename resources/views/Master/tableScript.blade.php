@section('css-list')
    @parent
    <!-- dataTable -->
    <link rel="stylesheet" href="adminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="adminLTE/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="adminLTE/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

    <!-- dropzonejs -->
    <link rel="stylesheet" href="adminLTE/plugins/dropzone/min/dropzone.min.css">
@endsection

@section('script')
    @parent
    <!-- dataTable -->
    <script src="adminLTE/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="adminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="adminLTE/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="adminLTE/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="adminLTE/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="adminLTE/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="adminLTE/plugins/jszip/jszip.min.js"></script>
    <script src="adminLTE/plugins/pdfmake/pdfmake.min.js"></script>
    <script src="adminLTE/plugins/pdfmake/vfs_fonts.js"></script>
    <script src="adminLTE/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="adminLTE/plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="adminLTE/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
    <!-- dropzonejs -->
    <script src="adminLTE/plugins/dropzone/min/dropzone.min.js"></script>

    <!-- Page specific script -->
    <script>
      $(function () {
        $("#example1").DataTable({
          "responsive": true, "lengthChange": false, "autoWidth": false,
          "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false,
          "responsive": true,
        });
      });
    </script>
@endsection
