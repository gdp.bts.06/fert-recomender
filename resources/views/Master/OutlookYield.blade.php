@extends('layout.base')
@section('content')

@include('Master.Component.dropzoneCard')
<div class="row">
  <div class="col-md-12">
    <!-- upload form -->
    <div class="card card-default">
      <div class="card-header">
        <h3 class="card-title">Import excel file</h3>
      </div>
      <div class="card-body">
      <form action="/import-outlook-yield" method="post" enctype="multipart/form-data">
          <input type="file" name="file" required/>
          @csrf
          <button class="btn btn-success" type="submit">Submit</button>
      </form>
      </div>
      <div class="card-footer">
        Browse or drag file here 
      </div>
    </div>
  </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
            <h3 class="card-title">Outlook Yield</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>KODE BA</th>
                            <th>KODE BLOK</th>
                            <th>BLOK DESC</th>
                            <th>YIELD(TON/HA)</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(!empty($data[0]))
                        @foreach($data[0] as $data)
                            <tr>
                                <td>{{ $data[0] }}</td>
                                <td>{{ $data[1] }}</td>
                                <td>{{ $data[2] }}</td>
                                <td>{{ $data[3] }}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
</div>
@endsection

@include('Master.tableScript')