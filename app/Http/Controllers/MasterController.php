<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\fileImport;
use App\Http\Controllers\Controller;
use Response;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class MasterController extends Controller
{
    public function Upload(Request $request)
    {
        $file       = $request->file('file');
        $lokasi     = public_path().'/upload'.'/'.date('Y-m-d');
        $fileName   = Str::random(4).'_'.$file->getClientOriginalName();
        $upload_success = $request->file('file')->move($lokasi, $fileName);

        if ($upload_success) {
            return  response('Success ', 200);
        } else {
            return  response('Error ', 400);
        }
        
    }
    // Hectar Statement file import
    public function HsImport(Request $request){
        $data = Excel::toCollection(new fileImport, request()->file('file'));
        return view('Master.MasterHS_BPS',['data'=> $data]);
    }

    // LSU file Import
    public function LsuImport(Request $request){
        $data = Excel::toCollection(new fileImport, request()->file('file'));
        return view('Master.LeafSamplingUnit',['data'=> $data]);
    }

    // import blok sap file
    public function BlokSapImport(Request $request){
        $data = Excel::toCollection(new fileImport, request()->file('file'));
        return view('Master.MasterBlockSAP',['data'=> $data]);
    }

    // import outlook yield file
    public function OutlookYieldImport(Request $request){
        $data = Excel::toCollection(new fileImport, request()->file('file'));
        return view('Master.OutlookYield',['data'=> $data]);
    }

    // import target yield file
    public function TargetYieldImport(Request $request){
        $data = Excel::toCollection(new fileImport, request()->file('file'));
        return view('Master.TargetYield3TH',['data'=> $data]);
    }

    // import dist pupuk file
    public function DistPupukImport(Request $request){
        $data = Excel::toCollection(new fileImport, request()->file('file'));
        return view('Master.DistribusiPupuk',['data'=> $data]);
    }

    public function TestApi(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|file',
        ]);
        $file       = $request->file('file');
        $fileName   = $file->getClientOriginalName();
        $path = Storage::putFileAs(
            'public/uploaded/master_data/'.$request['message'],
            $file,
            $fileName
        );

        if (!$path) {
            return response("cant save file",412);
        }
        return response("success", 200);
    }
}
